# Set ssh key of Jenkins server
read -p "Enter Key location (if left blank will use ch9_shared.pem key): " KEYLOC
if [ -z "$KEYLOC" ]
then 
    KEYLOC=~/.ssh/ch9_shared.pem
fi

# Set IP address of Jenkins server 
IPADDRESS=$1
if [ -z "$IPADDRESS" ]
then 
    read -p "You forgot to enter the IP, please enter: " IPADDRESS
fi

# SSH into Ubuntu machine and run commands
ssh -o StrictHostKeyChecking=no -i $KEYLOC ubuntu@$IPADDRESS << EOF

# Update server and install Java 11
sudo apt update
sudo apt install openjdk-11-jdk -y

# Add the Jenkins repository key to the system  
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
# Add the debian package to sources list 
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

# Run update so apt uses new repo and install Jenkins 
sudo apt update
sudo apt install jenkins -y

# Start jenkins and verify this 
sudo systemctl start jenkins
systemctl status jenkins

# Gets the initial admin log in password
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

EOF


dynamicdevops
cohort9